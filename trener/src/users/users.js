
const users = angular.module('users', ['users.service'])

/**
 * @typedef User
 * @property {string} username
 */

/**
 * @typedef MyScope
 * @property {User} [selected]
 * @property {User} [draft]
 */

angular.module('users')
  .controller('UsersPageCtrl', [
    '$scope', '$timeout', '$q', 'UsersService',
    ($scope, $timeout, $q, UsersService) => {
      /** @type MyScope */
      // const this = $scope.usersPage = {}
      
      this.selected = null
      this.draft

      this.refresh = () => {
        return UsersService.fetchUsers()
          .then((data) => this.users = data)
      }
      this.refresh()

      this.create = () => {
        this.selected = {}
        this.draft = {}
        this.mode = 'edit'
      }

      this.select = (id) => {
        return UsersService.fetchUserById(id).then(user => {
          this.selected = user
          this.mode = 'details'
        })
      }

      this.edit = () => {
        this.mode = 'edit'
        this.draft = { ...this.selected }
      }
      this.select('1').then(() => this.edit())

      this.save = (draft) => {
        if (this.userForm.$invalid) {
          this.showMessage('Form has errors')
          return
        }
        const result =
          draft.id ? UsersService.saveUpdatedUser(draft) : UsersService.saveNewUser(draft)

        return result.then(saved => {
          return $q.all([this.select(saved.id), this.refresh()])
        })
          .then(() => this.showMessage('Changes Saved'))
      }

      this.showMessage = msg => {
        this.message = msg;
        $timeout(() => {
          this.message = ''
        }, 2000)
      }

    }])
  // .controller('UserListCtrl', ($scope) => {
  //   const this = $scope.list = {}
  //   this.filtered = []
  //   this.select = (id) => $scope.$emit('selectUser', id)

  // })
  // .controller('UserDetailsCtrl', ($scope) => {
  //   const this = $scope.details = {}
  //   this.user = null

  //   $scope.$on('userSelected', (event, user) => { this.user = (user) })

  // })
  // .controller('UserEditFormCtrl', ($scope) => {
  //   const this = $scope.editform = {}
  //   this.draft = {}
  // })
