const tasks = angular.module("tasks", []);

// DCOMPONENTS
angular.module("tasks").component("tasksPage", {
  bindings: {},
  template: /* html */ `<div> TasksPage {{$ctrl.title}}
        <div class="row">
          <div class="col">
            <tasks-list tasks="$ctrl.tasks1" selected="$ctrl.selected" on-select="$ctrl.select($event)"></tasks-list>
          </div>
          <div class="col">
            <tasks-list tasks="$ctrl.tasks2" selected="$ctrl.selected" on-select="$ctrl.select($event)"></tasks-list>
          </div>
          <div class="col">
            <task-details task="$ctrl.selected" on-edit="$ctrl.edit($event)" on-cancel="$ctrl.cancel($event)" ng-if="$ctrl.selected">
            </task-details>

            <task-editor task="$ctrl.selected" ng-if="$ctrl.selected && $ctrl.mode == 'edit'" on-save="$ctrl.save($event)" on-remove="$ctrl.remove($event)"></task-editor>
          </div>
        </div>
      </div>`,
  controller() {
    // const vm = $scope.$ctrl = {}
    this.title = "Tasks";
    this.mode = "details";
    this.selected = null;
    // this.tasks = this.tasks_detalis;
    // this.lists = { 123: [], 234: [] };
    this.tasks1 = [
      { id: 1, name: "task1", userId: 1, completed: true },
      { id: 2, name: "task2", userId: 1, completed: true },
      { id: 3, name: "task3", userId: 1, completed: true },
    ];
    this.tasks2 = [
      { id: 4, name: "task4", userId: 2, completed: true },
      { id: 5, name: "task5", userId: 2, completed: true },
      { id: 6, name: "task6", userId: 2, completed: true },
    ];
    this.select = (task) => {
      this.selected = task;
      this.mode = "details";
    };
    this.cancel = (event) => {
      this.selected = null;
      this.mode = "details";
    };
    this.edit = (event) => {
      this.mode = "edit";
    };
    this.save = (event) => {
      const seelctedList = event.userId == 1 ? this.tasks1 : this.tasks2;
      const index = seelctedList.findIndex((i) => i.id === event.id);
      seelctedList[index] = event;
      this.mode = "details";
      this.selected = null;
    };
    this.remove = (event) => {
      let seelctedList = event.userId == 1 ? this.tasks1 : this.tasks2;
      // this.tasks = this.tasks.filter((i) => i.id != event.id);
      // console.log(seelctedList.filter((i) => i.id != event.id));
      seelctedList = seelctedList.filter((i) => i.id != event.id);
      event.userId == 1 ? this.tasks1 = seelctedList: this.tasks2 = seelctedList;
 
      this.mode = "details";
      this.selected = null;
    };
  },
});
angular.module("tasks").component("tasksList", {
  bindings: { tasks: "=", onSelect: "&", selected: "<" }, // on-select=" zyx = $event.completed "
  template: /* html */ `
        <div> List: 
          <div class="list-group">
            <div class="list-group-item" ng-repeat="task in $ctrl.tasks" 
              ng-class="{active: task.id == $ctrl.selected.id}"
              ng-click="$ctrl.onSelect({$event: task});"
              ng-model="task">
            {{$index+1}}. {{task.name}}, Completed: {{task.completed}}
            </div>
          </div>
        </div>`,
  controller() {},
});

angular.module("tasks").component("taskDetails", {
  bindings: { task: "=", onEdit: "&", onCancel: "&" },
  template: /* html */ `<div>tasksDetails <dl>
      <dt>Name</dt>
      <dd>{{$ctrl.task.name}}</dd>
    </dl>
      <button ng-click="$ctrl.onEdit({$event: $ctrl.task})">Edit</button>
      <button ng-click="$ctrl.onCancel({$event:$ctrl.task})">Cancel</button>
    </div>`,
  controller($rootScope, $http) {}, // this == $ctrl
});

angular.module("tasks").component("taskEditor", {
  bindings: { task: "=", onRemove: "&", onSave: "&" },
  template: /* html */ `<div> taskForm:
        <div class="form-group mb-3">
          <label for="task_name">Name</label>
          <input id="task_name" type="text" class="form-control" 
                ng-model="$ctrl.draft.name" ng-change="$ctrl.draft.name">
        </div>

        <div class="form-group mb-3">
          <label for="task_completed">
            <input id="task_completed" ng-model="$ctrl.draft.completed" ng-change="$ctrl.draft.completed" type="checkbox"> Completed</label>
        </div>

        <button ng-click="$ctrl.onSave({$event: $ctrl.draft})">Save</button>
        <button ng-click="$ctrl.onRemove({$event: $ctrl.task})">Cancel</button>
      </div>`,
  // controller:'TaskEditorCtrl as $ctrl'
  controller($scope) {
    this.$onInit = () => {
      this.draft = { ...this.task };
    };

    // https://docs.angularjs.org/guide/component#component-based-application-architecture
    this.$onChanges = (changes) => {
      this.draft = { ...this.task };
    };
    this.$doCheck = () => {}; // after parent $digest
    this.$postLink = () => {}; // after all child DOM is ready

    this.$onDestroy = () => {
      console.log("bye bye!");
    };
  },
  // controllerAs: '$ctrl'
});

// CONTROLLER
tasks.controller("TasksCtrl", ($scope) => {
  $scope.mode = "show";
  $scope.findItem = "";

  $scope.tasks = [
    {
      id: "123",
      name: "Zakupy",
      completed: true,
    },
    {
      id: "234",
      name: "Gotowanie",
      completed: false,
    },
    {
      id: "345",
      name: "Sprzątanie",
      completed: true,
    },
  ];

  $scope.task = [];

  $scope.countasks = () => {
    $scope.finished = 0;
    $scope.notFinished = 0;
    const mytable = [];
    $scope.tasks.forEach((element) => mytable.push(element.completed));
    for (i = 0; mytable.length - 1 >= i; i++) {
      if (mytable[i] == true) {
        $scope.finished = $scope.finished + 1;
      } else {
        $scope.notFinished = $scope.notFinished + 1;
      }
    }
  };

  $scope.countasks();

  $scope.editForm = () => {
    $scope.mode = "edit";
    $scope.taskNew = { ...$scope.task };
  };

  $scope.hideForm = () => {
    $scope.mode = "show";
  };

  $scope.saveForm = () => {
    if ($scope.taskNew.id === undefined) {
      $scope.taskNew.id = Date.now().toString();
      if ($scope.taskNew.completed === undefined) {
        $scope.taskNew.completed = false;
      }
      if ($scope.taskNew.name === undefined) {
        $scope.taskNew.name = "";
      }
      $scope.task = $scope.taskNew;
    } else {
      $scope.task = { ...$scope.taskNew };
    }

    $scope.mode = "show";
    const index = $scope.tasks.findIndex((i) => i.id === $scope.task.id);
    if (index === -1) {
      $scope.tasks.splice($scope.tasks[0], 0, $scope.task);
      console.log($scope.tasks);
    } else {
      /* 1 */ Object.assign($scope.tasks[index], $scope.task);
      // /* 2 */$scope.tasks[index] = $scope.task;
      // /* 3 */$scope.tasks = $scope.tasks.map((t) =>
      //   t.id == $scope.task.id ? $scope.task : t
      // );
    }

    $scope.countasks();
  };

  $scope.markItem = (id) => {
    $scope.task = $scope.tasks.find((i) => i.id === id);
    $scope.mode = "show";
    const count = $scope.tasks.find((i) => i.completed === true);
  };

  $scope.delPosiion = (id) => {
    $scope.task = $scope.tasks.find((i) => i.id === id);
    $scope.tasks = $scope.tasks.filter((i) => i != $scope.task);
    $scope.mode = "show";

    $scope.countasks();
  };

  $scope.findPosition = (findItem) => {
    const findedItem = $scope.tasks.find((i) => i.name.includes(findItem));
    $scope.markItem(findedItem.id);
  };

  $scope.addPosiion = () => {
    $scope.markItem("");
    $scope.editForm();
    $scope.countasks();
  };

  $scope.saveFormEvent = (event) => {
    if (event.which === 13) {
      $scope.saveForm();
    }
  };
});
