const users = angular.module("users", ["usersService"]);

users.controller("UsersCtrl", ($scope, UsersService) => {
  const vm = ($scope.usersPage = {});

  vm.selected = null;
  vm.userLogged = {};
  vm.editShow = false;
  vm.wordLength = false;
  vm.showMessage = false;

  // vm.users = UsersService.getUsers();

  vm.refresh = () => {
    UsersService.fetchUsers()
      .then((data) => {
        vm.users = data
      })
  }
  vm.refresh();


  vm.select = (id) => {
    UsersService.fetchUsers()
    .then((data) => {
      vm.users = data
    })
    vm.editShow = true;
    vm.selected = UsersService.getUserById(id);
  };

  // vm.validation = (word) =>{
  //   if(word.length < 3){
  //     console.log("to short");
  //     vm.wordLength = true;
  //     console.log(vm.wordLength)
  //   }
  //   else{
  //     console.log("ok");
  //     vm.wordLength = false;
  //     console.log(vm.wordLength)
  //   }
  // }

  vm.save = (draft, username) => {
    let x = username;
 
    if(vm.form.$invalid){
      vm.showMessage('Form has errors');
      console.log("błąd zapisu")
      return
    }

    if (draft === undefined) {
      vm.selected.id = Date.now().toString();
      if (vm.selected.username === undefined || vm.selected.username === "") {
        console.log("nie wpisano danych");
      } else {
        vm.selected = { ...vm.selected };
      }
      vm.selected = UsersService.addUser(vm.selected);

      var listmy = vm.refresh();
      console.log(listmy)
      // vm.users = UsersService.getUsers();
    } else {
      console.log(draft, "username", username);
      vm.selected = UsersService.updateUser(draft, username);
      vm.refresh();
      // vm.users = UsersService.getUsers();
    }
    vm.editShow = false;
  };

  vm.showMessage = (mess)=>{
    vm.showMessage = true;
    vm.messText = mess;
    alert(mess);
  }

  vm.closeMess = () =>{
    vm.showMessage = false;
  }



  vm.delete = (draft) => {
    UsersService.fetchUsers()
    .then((data) => {
      vm.users = data
    })
    vm.selected = UsersService.deleteUser(draft);
    // vm.users = UsersService.getUsers();
    vm.editShow = false;
    UsersService.fetchUsers()
    .then((data) => {
      vm.users = data
    })
  };


  vm.add = () => {
    vm.selected = {};
    vm.editShow = true;
  };


});

angular.module("usersService", []).service("UsersService", function ($q, $http) {
  this._users = [
    { id: "123", username: "Admin" },
    { id: "456", username: "Alice" },
  ];

  // this.getUsers = () => {
  //   // vm.refresh();
  //   return this._users;
  // };

  // this.fetchUsers = () => {
  //   const users = fetch("http://localhost:3000/users")
  //     .then((response) => response.json())
  //     .then((data) => {
  //       this._users = data;
  //       return data;
  //     });

  //   return users;
  // };
  // this.fetchUsers();


  this.fetchUsers = () => {
    return $http.get("http://localhost:3000/users")
      .then((response) => {
        return (this._users  =response.data)
      })
    }


  this.updateUser = (draft, usernameVal) => {
    const user = this._users.find((i) => i.id === draft);
    user.username = usernameVal;
    const index = this._users.findIndex((i) => i.id === draft);
    this._users[index] = user;
    // console.log("http://localhost:3000/users/"+user.id, user)
    return $http.put("http://localhost:3000/users/"+user.id, user)
    .then((response) => response.data)
  };

  this.deleteUser = (draft) => {
    const user = this._users.find((i) => i.id === draft);
    const index = this._users.findIndex((i) => i.id === draft);
    this._users[index] = user;
    this._users.splice(index, 1);
    return $http.delete("http://localhost:3000/users/"+ draft)
  };

  this.addUser = (draft) => {
    console.log("add user", draft)
    this._users.splice(0, -1, draft);
    return $http.post("http://localhost:3000/users/", draft)
    .then((response) => response.data)
  };

  this.getUserById = (id) => {
    const user = this._users.find((i) => i.id === id);
    return user;
  };
});
