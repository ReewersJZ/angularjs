const users = angular.module("users", ["usersService"]);


angular.module("users").component("usersPage", {
  bindings: {},
  template: /* html */ `
    <div class="row">
      <div class="col mb-5" >
        <h1>Angular {{title}}</h1>

        <div class="list-group-item" ng-repeat="user in $ctrl.users">
          <div class="row">
            <div class="col">{{$index+1}}.</div>
            <div class="col">ID: {{user.id}}</div>
            <div class="col">User name: {{user.username}}</div>
            <div class="col-6 text-end" >
              <button class=" btn btn-success" ng-click="$ctrl.select(user.id)">Wybierz</button>
            </div>
          </div>
        </div>
        <div class="col text-center m-5">
          <button ng-click="$ctrl.add()" class="btn btn-primary" >
            Add new
          </button>  
        </div>

        <div class="col-6" ng-model="editShow" ng-show="$ctrl.editShow === true">
          <div class="col">
            <form name="$ctrl.form" novalidate>
              <div class="form-group mb-3">
                <label for="task_name">User name</label>
                <input
                  id="task_name"
                  type="text"
                  class="form-control"
                  ng-model="$ctrl.selected.username"
                  ng-keypress="$ctrl.saveFormEvent($event)"
                  ng-change="findPosition($ctrl.uselected.id)"
                  required minlength="3"
                  name="name"
                  
                />
              </div>
              <div ng-if="$ctrl.form['name'].$dirty || $ctrl.form['name'].$touched || $ctrl.form.$submitted">
                <p class="text-danger" ng-if="$ctrl.form['name'].$error.minlength">Name is too short!</p>
                <p class="text-danger" ng-if="$ctrl.form['name'].$error.required">Name is required!</p>
              </div>
              <button ng-click="$ctrl.delete($ctrl.selected.id)" class="btn btn-danger">
                Cancel
              </button>
              <button ng-click="$ctrl.save($ctrl.selected.id, $ctrl.selected.username)" class="btn btn-warning" >
                Save
              </button>
            </form>
          </div>
        </div>
    </div>

    <div class="alert_div" ng-model="$ctrl.messText" ng-show="$ctrl.showMessage === true">
      
      <p class="mt-5">{{$ctrl.messText}}</p>
      <button ng-click="$ctrl.closeMess()" class="btn btn-success mt-5" >
        OK
      </button>
    </div>`,

  controller(UsersService){
    this.selected = null;
    this.userLogged = {};
    this.editShow = false;
    this.wordLength = false;
    this.showMessage = false;

 
      this.refresh = () => {
        UsersService.fetchUsers()
          .then((data) => {
            this.users = data
          })
      }
      this.refresh();


      this.select = (id) => {
        UsersService.fetchUsers()
        .then((data) => {
          this.users = data
        })
        this.editShow = true;
        this.selected = UsersService.getUserById(id);
      };


      this.save = (draft, username) => {
        let x = username;
    
        if(this.form.$invalid){
          this.showMessage('Form has errors');
          console.log("błąd zapisu")
          return
        }

        if (draft === undefined) {
          this.selected.id = Date.now().toString();
          if (this.selected.username === undefined || this.selected.username === "") {
            console.log("nie wpisano danych");
          } else {
            this.selected = { ...this.selected };
          }
          this.selected = UsersService.addUser(this.selected);

          var listmy = this.refresh();
          console.log(listmy)
      
        } else {
          console.log(draft, "username", username);
          this.selected = UsersService.updateUser(draft, username);
          this.refresh();

        }
        this.editShow = false;
      };

      this.showMessage = (mess)=>{
        this.showMessage = true;
        this.messText = mess;
        alert(mess);
      }

      this.closeMess = () =>{
        this.showMessage = false;
      }



      this.delete = (draft) => {
        UsersService.fetchUsers()
        .then((data) => {
          this.users = data
        })
        this.selected = UsersService.deleteUser(draft);
        this.editShow = false;
        UsersService.fetchUsers()
        .then((data) => {
          this.users = data
        })
      };


      this.add = () => {
        this.selected = {};
        this.editShow = true;
      };
      }
    })




//     users.controller("UsersCtrl", ($scope, UsersService) => {
//       const this = ($scope.$ctrl = {});

//       this.selected = null;
//       this.userLogged = {};
//       this.editShow = false;
//       this.wordLength = false;
//       this.showMessage = false;

    

//       this.refresh = () => {
//         UsersService.fetchUsers()
//           .then((data) => {
//             this.users = data
//           })
//       }
//       this.refresh();


//       this.select = (id) => {
//         UsersService.fetchUsers()
//         .then((data) => {
//           this.users = data
//         })
//         this.editShow = true;
//         this.selected = UsersService.getUserById(id);
//       };


//       this.save = (draft, username) => {
//         let x = username;
    
//         if(this.form.$invalid){
//           this.showMessage('Form has errors');
//           console.log("błąd zapisu")
//           return
//         }

//         if (draft === undefined) {
//           this.selected.id = Date.now().toString();
//           if (this.selected.username === undefined || this.selected.username === "") {
//             console.log("nie wpisano danych");
//           } else {
//             this.selected = { ...this.selected };
//           }
//           this.selected = UsersService.addUser(this.selected);

//           var listmy = this.refresh();
//           console.log(listmy)
      
//         } else {
//           console.log(draft, "username", username);
//           this.selected = UsersService.updateUser(draft, username);
//           this.refresh();

//         }
//         this.editShow = false;
//       };

//       this.showMessage = (mess)=>{
//         this.showMessage = true;
//         this.messText = mess;
//         alert(mess);
//       }

//       this.closeMess = () =>{
//         this.showMessage = false;
//       }



//       this.delete = (draft) => {
//         UsersService.fetchUsers()
//         .then((data) => {
//           this.users = data
//         })
//         this.selected = UsersService.deleteUser(draft);
//         this.editShow = false;
//         UsersService.fetchUsers()
//         .then((data) => {
//           this.users = data
//         })
//       };


//       this.add = () => {
//         this.selected = {};
//         this.editShow = true;
//   };


// });

angular.module("usersService", []).service("UsersService", function ($q, $http) {
  this._users = [
    { id: "123", username: "Admin" },
    { id: "456", username: "Alice" },
  ];




  this.fetchUsers = () => {
    return $http.get("http://localhost:3000/users")
      .then((response) => {
        return (this._users  =response.data)
      })
    }


  this.updateUser = (draft, usernameVal) => {
    const user = this._users.find((i) => i.id === draft);
    user.username = usernameVal;
    const index = this._users.findIndex((i) => i.id === draft);
    this._users[index] = user;
    // console.log("http://localhost:3000/users/"+user.id, user)
    return $http.put("http://localhost:3000/users/"+user.id, user)
    .then((response) => response.data)
  };

  this.deleteUser = (draft) => {
    const user = this._users.find((i) => i.id === draft);
    const index = this._users.findIndex((i) => i.id === draft);
    this._users[index] = user;
    this._users.splice(index, 1);
    return $http.delete("http://localhost:3000/users/"+ draft)
  };

  this.addUser = (draft) => {
    console.log("add user", draft)
    this._users.splice(0, -1, draft);
    return $http.post("http://localhost:3000/users/", draft)
    .then((response) => response.data)
  };

  this.getUserById = (id) => {
    const user = this._users.find((i) => i.id === id);
    return user;
  };
});


{/* <div class="row" ng-controller="UsersCtrl">
        <div class="col mb-5" >
          <h1>Angular {{title}}</h1>

          <div class="list-group-item" ng-repeat="user in $ctrl.users">
            <div class="row">
              <div class="col">{{$index+1}}.</div>
              <div class="col">ID: {{user.id}}</div>
              <div class="col">User name: {{user.username}}</div>
              <div class="col-6 text-end" >
                <button class=" btn btn-success" ng-click="$ctrl.select(user.id)">Wybierz</button>
              </div>
            </div>
          </div>
          <div class="col text-center m-5">
            <button ng-click="$ctrl.add()" class="btn btn-primary" >
              Add new
            </button>  
          </div>

          <div class="col-6" ng-model="editShow" ng-show="$ctrl.editShow === true">
            <div class="col">
              <form name="$ctrl.form" novalidate>
                <div class="form-group mb-3">
                  <label for="task_name">User name</label>
                  <input
                    id="task_name"
                    type="text"
                    class="form-control"
                    ng-model="$ctrl.selected.username"
                    ng-keypress="$ctrl.saveFormEvent($event)"
                    ng-change="findPosition($ctrl.uselected.id)"
                    required minlength="3"
                    name="name"
                    
                  />
                </div>
                <div ng-if="$ctrl.form['name'].$dirty || $ctrl.form['name'].$touched || $ctrl.form.$submitted">
                  <p class="text-danger" ng-if="$ctrl.form['name'].$error.minlength">Name is too short!</p>
                  <p class="text-danger" ng-if="$ctrl.form['name'].$error.required">Name is required!</p>
                </div>
                <button ng-click="$ctrl.delete($ctrl.selected.id)" class="btn btn-danger">
                  Cancel
                </button>
                <button ng-click="$ctrl.save($ctrl.selected.id, $ctrl.selected.username)" class="btn btn-warning" >
                  Save
                </button>



                <pre>{{$ctrl.form | json:2}}</pre>
              </form>
            </div>
          </div>


         


        </div>

        <div class="alert_div" ng-model="$ctrl.messText" ng-show="$ctrl.showMessage === true">
          
          <p class="mt-5">{{$ctrl.messText}}</p>
          <button ng-click="$ctrl.closeMess()" class="btn btn-success mt-5" >
            OK
          </button>
        </div>
    </div> */}
     