const settings = angular.module("settings", ['settingsService']);


// CONTROLLER
settings.controller("SettingsCtrl", ($scope, SettingsService) => {

    const vm = $scope.settingsPage = {};
  
    vm.settingsLabel = 'settings';


  })



  // DIRECTIVE
  settings.directive("appHighlight", function (PAGES){

    return{
      restrict: 'EACM',
      link(scope, $element, attrs, ctrl){
        console.log('Hello', $element);

        $element.css('color', 'red').html('<p>placki</p>').on('click', el => $element.find('p').css('color', 'blue'));
      }
    }


  });


  
  // SERVIS
  angular.module("settingsService", []).service('SettingsService', function(){})



  angular.module('settings').directive('appAlert', function () {
    return {
      scope: {message: "@", type: "@", onDismiss: "&"},
      template: /* html */`<div class="alert alert-{{type}}" ng-if="$alert.open">
      <span class="close float-end mouse-cursor" ng-click="$alert.click()" >&times;</span>
        <span>{{message}}<span>
      </div>`,


    controller($scope) {
      const vm = $scope.$alert = {}
      vm.open = true;
      vm.zmienna = null;
      vm.wiadomosc = null;
      vm.mess = null;
      console.log("Tutaj wyświetlam $scope.message - jest to parent: ", vm.mess)
      console.log($scope.type)
      vm.click = () => {vm.open = false; $scope.onDismiss()}

    }
    }
  })