
// console.log('Hello Angular')
// console.log(angular.version)

angular.module('myApp', ['tasks', 'users', 'settings']).constant('PAGES', [
    { name: 'tasks', label: 'Tasks', url: 'views/tasks-view.tpl.html' },
    { name: 'users', label: 'Users', url: 'views/users-view.tpl.html' },
    { name: 'settings', label: 'Settings', url: 'views/settings-view.tpl.html' }
  ]);



const app = angular.module('myApp')

app.run(function($rootScope){
    // $rootScope.title = 'my new App';

});

app.controller('AppCtrl', ($scope, PAGES) => {
    $scope.title = 'my new App';
    $scope.user = {name: 'Guest'};
    $scope.isLoggedIn = false;
    $scope.isWisible = 'task';
    $scope.pages = PAGES;
    $scope.currentPage = $scope.pages[1];
    // console.log($scope.currentPage)


    $scope.changePage = (pageNumber) =>{
        $scope.currentPage = $scope.pages[pageNumber];
    }



    $scope.login = () => {
        $scope.user.name = 'Admin';
        $scope.isLoggedIn = true
    }

    $scope.logout = () => {
        $scope.user.name = 'Guest';
        $scope.isLoggedIn =false
    }

    $scope.showCart = () =>{
        console.log("show")
    }

        $scope.showCart = (param)=>{
        $scope.isWisible = param
    }
})


